--[[
lvim is the global options object

Linters should be
filled in as strings with either
a global executable or a path to
an executable
]]
-- THESE ARE EXAMPLE CONFIGS FEEL FREE TO CHANGE TO WHATEVER YOU WANT

-- general
lvim.log.level = "warn"
lvim.format_on_save.enabled = false
lvim.colorscheme = "lunar"
vim.opt.relativenumber = true
vim.opt.shiftwidth = 4 -- the number of spaces inserted for each indentation
vim.opt.tabstop = 4    -- insert 2 spaces for a tab
vim.opt.fileencoding = "utf-8"
-- to disable icons and use a minimalist setup, uncomment the following
-- lvim.use_icons = false

-- keymappings [view all the defaults by pressing <leader>Lk]
lvim.leader = "space"
-- add your own keymapping
lvim.keys.normal_mode["<C-s>"] = ":w<cr>"
-- lvim.keys.normal_mode["<C-x>h"] = ":split"
lvim.keys.insert_mode["jj"] = "<esc>"
lvim.keys.term_mode["yy"] = "<C-\\><C-n>"
-- lvim.keys.term_mode["jj"] = "<cmd>normal<cr>"

lvim.builtin.terminal.auto_scroll = false

-- lvim.keys.normal_mode["<S-l>"] = ":BufferLineCycleNext<CR>"
-- lvim.keys.normal_mode["<S-h>"] = ":BufferLineCyclePrev<CR>"
-- unmap a default keymapping
-- vim.keymap.del("n", "<C-Up>")
-- override a default keymapping
-- lvim.keys.normal_mode["<C-q>"] = ":q<cr>" -- or vim.keymap.set("n", "<C-q>", ":q<cr>" )
--
-- Change Telescope navigation to use j and k for navigation and n and p for history in both input and normal mode.
-- we use protected-mode (pcall) just in case the plugin wasn't loaded yet.
-- local _, actions = pcall(require, "telescope.actions")
-- lvim.builtin.telescope.defaults.mappings = {
--   -- for input mode
--   i = {
--     ["<C-j>"] = actions.move_selection_next,
--     ["<C-k>"] = actions.move_selection_previous,
--     ["<C-n>"] = actions.cycle_history_next,
--     ["<C-p>"] = actions.cycle_history_prev,
--   },
--   -- for normal mode
--   n = {
--     ["<C-j>"] = actions.move_selection_next,
--     ["<C-k>"] = actions.move_selection_previous,
--   },
-- }

-- Change theme settings
-- lvim.builtin.theme.options.dim_inactive = true
-- lvim.builtin.theme.options.style = "storm"

-- lvim.builtin.which_key.mappings["H"] = lvim.builtin.which_key.mappings["h"]
lvim.builtin.which_key.mappings["h"] = {
    name = "[h]arpoon",
    h = {"<cmd>lua require('harpoon.ui').toggle_quick_menu()<cr>", "[h]arpoon ui"},
    a = {"<cmd>lua require('harpoon.mark').add_file()<cr>", "[a]dd file"},
    j = {"<cmd>lua require('harpoon.ui').nav_next()<cr>", "[j] next file"},
    k = {"<cmd>lua require('harpoon.ui').nav_prev()<cr>", "[k] prev file"},
    ["1"] = {"<cmd>lua require('harpoon.ui').nav_file(1)<cr>"},
    ["2"] = {"<cmd>lua require('harpoon.ui').nav_file(2)<cr>"},
    ["3"] = {"<cmd>lua require('harpoon.ui').nav_file(3)<cr>"},
    ["4"] = {"<cmd>lua require('harpoon.ui').nav_file(4)<cr>"},
    ["5"] = {"<cmd>lua require('harpoon.ui').nav_file(5)<cr>"},
    ["6"] = {"<cmd>lua require('harpoon.ui').nav_file(6)<cr>"},
    ["7"] = {"<cmd>lua require('harpoon.ui').nav_file(7)<cr>"},
    ["8"] = {"<cmd>lua require('harpoon.ui').nav_file(8)<cr>"},
    ["9"] = {"<cmd>lua require('harpoon.ui').nav_file(9)<cr>"},
}


-- Use which-key to add extra bindings with the leader-key prefix
lvim.builtin.which_key.mappings["A"] = { "<cmd>ASToggle<cr>", "[A]utosave toggle" }
lvim.builtin.which_key.mappings["P"] = { "<cmd>Telescope projects<CR>", "[P]rojects" }
lvim.builtin.which_key.mappings["B"] = { "<cmd>DBUIToggle<cr>", "Dad[B]od UI" }
lvim.builtin.which_key.mappings["S"] = {
    name = "[S]ession",
    c = { "<cmd>lua require('persistence').load()<cr>", "Restore last session for [c]urrent dir" },
    l = { "<cmd>lua require('persistence').load({ last = true })<cr>", "Restore [l]ast session" },
    Q = { "<cmd>lua require('persistence').stop()<cr>", "[Q]uit without saving session" },
}
lvim.builtin.which_key.mappings["t"] = {
    name = "+[t]erminal",
    ["1"] = { "<cmd>1ToggleTerm<cr>", "Open terminal [1]" },
    ["2"] = { "<cmd>2ToggleTerm<cr>", "Open terminal [2]" },
    ["3"] = { "<cmd>3ToggleTerm<cr>", "Open terminal [3]" },
    ["4"] = { "<cmd>4ToggleTerm<cr>", "Open terminal [4]" },
    f = { "<cmd>ToggleTerm<cr>", "[f]loating terminal" },
    v = { "<cmd>2ToggleTerm size=30 direction=vertical<cr>", "Split [v]ertical" },
    h = { "<cmd>2ToggleTerm size=30 direction=horizontal<cr>", "Split [h]orizontal" },
}
lvim.builtin.which_key.mappings["r"] = {
    name = "+t[r]ouble",
    r = { "<cmd>Trouble lsp_references<cr>", "[r]eferences" },
    f = { "<cmd>Trouble lsp_definitions<cr>", "De[f]initions" },
    d = { "<cmd>Trouble document_diagnostics<cr>", "[d]iagnostics" },
    q = { "<cmd>Trouble quickfix<cr>", "[q]uickFix" },
    l = { "<cmd>Trouble loclist<cr>", "[l]ocationList" },
    w = { "<cmd>Trouble workspace_diagnostics<cr>", "[w]orkspace Diagnostics" },
}
lvim.builtin.which_key.mappings["x"] = {
    name="+[x] Split",
    v = {"<cmd>vsplit<cr>", "Split [v]ertical"},
    h = {"<cmd>split<cr>", "Split [h]orizontal"},
}


lvim.builtin.which_key.mappings["D"] = { "[D]ogeGenerate<cr>" }

lvim.builtin.alpha.active = true
lvim.builtin.alpha.mode = "dashboard"
lvim.builtin.terminal.active = true
lvim.builtin.nvimtree.setup.view.side = "left"
lvim.builtin.nvimtree.setup.renderer.icons.show.git = true

-- if you don't want all the parsers change this to a table of the ones you want
lvim.builtin.treesitter.ensure_installed = {
    "bash",
    "c",
    "javascript",
    "json",
    "lua",
    "python",
    "typescript",
    "tsx",
    "css",
    "rust",
    "java",
    "yaml",
}

lvim.builtin.treesitter.ignore_install = { "haskell" }
lvim.builtin.treesitter.highlight.enable = true

-- lvim.doge_enable_mappings = 0

lvim.doge_doc_standard_python = 'reST'


-- generic LSP settings

-- -- make sure server will always be installed even if the server is in skipped_servers list
-- lvim.lsp.installer.setup.ensure_installed = {
--     "sumneko_lua",
--     "jsonls",
-- }
-- -- change UI setting of `LspInstallInfo`
-- -- see <https://github.com/williamboman/nvim-lsp-installer#default-configuration>
-- lvim.lsp.installer.setup.ui.check_outdated_servers_on_open = false
-- lvim.lsp.installer.setup.ui.border = "rounded"
-- lvim.lsp.installer.setup.ui.keymaps = {
--     uninstall_server = "d",
--     toggle_server_expand = "o",
-- }

-- ---@usage disable automatic installation of servers
-- lvim.lsp.installer.setup.automatic_installation = false

-- ---configure a server manually. !!Requires `:LvimCacheReset` to take effect!!
-- ---see the full default list `:lua print(vim.inspect(lvim.lsp.automatic_configuration.skipped_servers))`
-- vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "pyright" })
-- local opts = {} -- check the lspconfig documentation for a list of all possible options
-- require("lvim.lsp.manager").setup("pyright", opts)

-- ---remove a server from the skipped list, e.g. eslint, or emmet_ls. !!Requires `:LvimCacheReset` to take effect!!
-- ---`:LvimInfo` lists which server(s) are skipped for the current filetype
-- lvim.lsp.automatic_configuration.skipped_servers = vim.tbl_filter(function(server)
--   return server ~= "emmet_ls"
-- end, lvim.lsp.automatic_configuration.skipped_servers)

-- -- you can set a custom on_attach function that will be used for all the language servers
-- -- See <https://github.com/neovim/nvim-lspconfig#keybindings-and-completion>
-- lvim.lsp.on_attach_callback = function(client, bufnr)
--   local function buf_set_option(...)
--     vim.api.nvim_buf_set_option(bufnr, ...)
--   end
--   --Enable completion triggered by <c-x><c-o>
--   buf_set_option("omnifunc", "v:lua.vim.lsp.omnifunc")
-- end

-- -- set a formatter, this will override the language server formatting capabilities (if it exists)
-- local formatters = require "lvim.lsp.null-ls.formatters"
-- formatters.setup {
--   { command = "black", filetypes = { "python" } },
--   { command = "isort", filetypes = { "python" } },
--   {
--     -- each formatter accepts a list of options identical to https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md#Configuration
--     command = "prettier",
--     ---@usage arguments to pass to the formatter
--     -- these cannot contain whitespaces, options such as `--line-width 80` become either `{'--line-width', '80'}` or `{'--line-width=80'}`
--     extra_args = { "--print-with", "100" },
--     ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
--     filetypes = { "typescript", "typescriptreact" },
--   },
-- }

-- -- set additional linters
-- local linters = require "lvim.lsp.null-ls.linters"
-- linters.setup {
--   { command = "flake8", filetypes = { "python" } },
--   {
--     -- each linter accepts a list of options identical to https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md#Configuration
--     command = "shellcheck",
--     ---@usage arguments to pass to the formatter
--     -- these cannot contain whitespaces, options such as `--line-width 80` become either `{'--line-width', '80'}` or `{'--line-width=80'}`
--     extra_args = { "--severity", "warning" },
--   },
--   {
--     command = "codespell",
--     ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
--     filetypes = { "javascript", "python" },
--   },
-- }



vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "pyright" })

local root_files = {
    "pyproject.toml",
    "setup.py",
    "setup.cfg",
    "requirements.txt",
    "Pipfile",
    "manage.py",
    "pyrightconfig.json",
    ".git",
    ".gitignore",
}

local opts = {
    root_dir = require("lspconfig.util").root_pattern(unpack(root_files)),
    single_file_support = true,
    filetypes = { "python" },
    cmd = { "pyright-langserver", "--stdio" },
    settings = {
        pyright = {
            disableOrganizeImports = false,
            disableLanguageServices = false,
        },
        python = {
            venvPath = "/opt/miniconda/envs/",
            pythonPath = "/usr/bin/python3",
            analysis = {
                logLevel = "Information",
                -- stubPath = "$HOME/Documents/typings",
                extraPaths = {},
                typeshedPaths = {},
                diagnosticMode = "workspace",
                autoSearchPaths = true,
                typeCheckingMode = "basic",
                autoImportCompletions = true,
                useLibraryCodeForTypes = true,
                diagnosticSeverityOverrides = {},
            }
        },
    }
}

require("lvim.lsp.manager").setup("pyright", opts)



-- Additional Plugins
lvim.plugins = {
    { "lambdalisue/suda.vim" },
    { "tpope/vim-surround" },
    -- {"neovim/nvim-lspconfig"},
    -- {"nvim-lua/plenary.nvim"},
    -- {"mfussenegger/nvim-dap"},
    {
        "simrat39/rust-tools.nvim",
        config = function()
            local status_ok, rust_tools = pcall(require, "rust-tools")
            if not status_ok then
                return
            end

            local rust_opts = {
                tools = {
                    executor = require("rust-tools/executors").termopen, -- can be quickfix or termopen
                    reload_workspace_from_cargo_toml = true,
                    inlay_hints = {
                        auto = true,
                        only_current_line = false,
                        show_parameter_hints = true,
                        parameter_hints_prefix = "<-",
                        other_hints_prefix = "=>",
                        max_len_align = false,
                        max_len_align_padding = 1,
                        right_align = false,
                        right_align_padding = 7,
                        highlight = "Comment",
                    },
                    hover_actions = {
                        border = {
                            { "╭", "FloatBorder" },
                            { "─", "FloatBorder" },
                            { "╮", "FloatBorder" },
                            { "│", "FloatBorder" },
                            { "╯", "FloatBorder" },
                            { "─", "FloatBorder" },
                            { "╰", "FloatBorder" },
                            { "│", "FloatBorder" },
                        },
                        auto_focus = true,
                    },
                },
                server = {
                    on_attach = require("lvim.lsp").common_on_attach,
                    on_init = require("lvim.lsp").common_on_init,
                    settings = {
                        ["rust-analyzer"] = {
                            checkOnSave = {
                                command = "clippy"
                            }
                        }
                    },
                },
            }
            --local extension_path = vim.fn.expand "~/" .. ".vscode/extensions/vadimcn.vscode-lldb-1.7.3/"

            --local codelldb_path = extension_path .. "adapter/codelldb"
            --local liblldb_path = extension_path .. "lldb/lib/liblldb.dylib"

            --opts.dap = {
            --        adapter = require("rust-tools.dap").get_codelldb_adapter(codelldb_path, liblldb_path),
            --}
            rust_tools.setup(rust_opts)
        end,
        ft = { "rust", "rs" },
    },
    {
        "folke/persistence.nvim",
        event = "BufReadPre", -- this will only start session saving when an actual file was opened
        module = "persistence",
        config = function()
            require("persistence").setup {
                dir = vim.fn.expand(vim.fn.stdpath "config" .. "/session/"),
                options = { "buffers", "curdir", "tabpages", "winsize" },
            }
        end,
    },
    {
        "folke/trouble.nvim",
        cmd = "TroubleToggle",
    },
    {
        "folke/todo-comments.nvim",
        event = "BufRead",
        config = function()
            require("todo-comments").setup()
        end,
    },
    {
        "karb94/neoscroll.nvim",
        event = "WinScrolled",
        config = function()
            require('neoscroll').setup({
                -- All these keys will be mapped to their corresponding default scrolling animation
                mappings = { '<C-u>', '<C-d>', '<C-b>', '<C-f>',
                    '<C-y>', '<C-e>', 'zt', 'zz', 'zb' },
                hide_cursor = true,          -- Hide cursor while scrolling
                stop_eof = true,             -- Stop at <EOF> when scrolling downwards
                use_local_scrolloff = false, -- Use the local scope of scrolloff instead of the global scope
                respect_scrolloff = false,   -- Stop scrolling when the cursor reaches the scrolloff margin of the file
                cursor_scrolls_alone = true, -- The cursor will keep on scrolling even if the window cannot scroll further
                easing_function = nil,       -- Default easing function
                pre_hook = nil,              -- Function to run before the scrolling animation starts
                post_hook = nil,             -- Function to run after the scrolling animation ends
            })
        end
    },
    {
        "ray-x/lsp_signature.nvim",
        event = "BufRead",
        config = function() require "lsp_signature".on_attach() end,
    },
    {
        "windwp/nvim-ts-autotag",
        config = function()
            require("nvim-ts-autotag").setup()
        end,
    },
    {
        "JoosepAlviste/nvim-ts-context-commentstring",
        event = "BufRead",
    },
    {
        "rmagatti/goto-preview",
        config = function()
            require('goto-preview').setup {
                width = 120,             -- Width of the floating window
                height = 25,             -- Height of the floating window
                default_mappings = true, -- Bind default mappings
                debug = false,           -- Print debug information
                opacity = nil,           -- 0-100 opacity level of the floating window where 100 is fully transparent.
                post_open_hook = nil     -- A function taking two arguments, a buffer and a window to be ran as a hook.
                -- You can use "default_mappings = true" setup option
                -- Or explicitly set keybindings
                -- vim.cmd("nnoremap gpd <cmd>lua require('goto-preview').goto_preview_definition()<CR>")
                -- vim.cmd("nnoremap gpi <cmd>lua require('goto-preview').goto_preview_implementation()<CR>")
                -- vim.cmd("nnoremap gP <cmd>lua require('goto-preview').close_all_win()<CR>")
            }
        end
    },
    {
        "kkoomen/vim-doge",
        run = ':call doge#install()'
    },
    {
        "Pocco81/auto-save.nvim",
        config = function()
            require("auto-save").setup()
        end,
    },
    { "tpope/vim-dadbod" },
    { "kristijanhusak/vim-dadbod-ui" },
    { "kristijanhusak/vim-dadbod-completion" },
    { "vimwiki/vimwiki" },
    { 'chipsenkbeil/vimwiki-server.nvim',    tag = "v0.1.0-alpha.5" },
    {
        'ThePrimeagen/harpoon',
        config = function ()
            require("harpoon").setup({
                menu = {
                    width = vim.api.nvim_win_get_width(0) - 4,
                }
            })
        end
    },
}



table.insert(lvim.builtin.cmp.sources, { name = 'vim-dadbod-completion' })

if vim.g.neovide == true then
    vim.g.neovide_scale_factor = 0.65
    vim.api.nvim_set_keymap("n", "<C-+>", ":lua vim.g.neovide_scale_factor = vim.g.neovide_scale_factor + 0.1<CR>",
        { silent = true })
    vim.api.nvim_set_keymap("n", "<C-->", ":lua vim.g.neovide_scale_factor = vim.g.neovide_scale_factor - 0.1<CR>",
        { silent = true })
    vim.api.nvim_set_keymap("n", "<C-0>", ":lua vim.g.neovide_scale_factor = 1<CR>", { silent = true })
end

-- Autocommands (https://neovim.io/doc/user/autocmd.html)
-- vim.api.nvim_create_autocmd("BufEnter", {
--   pattern = { "*.json", "*.jsonc" },
--   -- enable wrap mode for json files only
--   command = "setlocal wrap",
-- })
-- vim.api.nvim_create_autocmd("FileType", {
--   pattern = "zsh",
--   callback = function()
--     -- let treesitter use bash highlight for zsh files as well
--     require("nvim-treesitter.highlight").attach(0, "bash")
--   end,
-- })

-- lvim.autocommands = {
--     {
--         { "BufEnter", "Filetype" },
--         {
--             desc = "Open mini.map and exclude some filetypes",
--             pattern = { "*" },
--             callback = function()
--                 local exclude_ft = {
--                     "qf",
--                     "NvimTree",
--                     "toggleterm",
--                     "TelescopePrompt",
--                     "alpha",
--                     "netrw",
--                 }

--                 local map = require('mini.map')
--                 if vim.tbl_contains(exclude_ft, vim.o.filetype) then
--                     vim.b.minimap_disable = true
--                     map.close()
--                 elseif vim.o.buftype == "" then
--                     map.open()
--                 end
--             end,
--         },
--     },
-- }
