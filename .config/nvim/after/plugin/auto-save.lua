require('auto-save').setup({})

vim.keymap.set("n", "<leader>A", "<cmd>ASToggle<CR>", {})

local svenutils = require("sven-utils")
if svenutils.isModuleAvailable("which-key") then
	local wk = require("which-key")
	wk.register({
        A = "[A]utosave toggle",
    }, {prefix = "<leader>"})
end
