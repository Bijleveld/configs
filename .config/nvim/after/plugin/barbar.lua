local svenutils = require("sven-utils")
if not svenutils.isModuleAvailable('barbar') then
    return
end


vim.g.barbar_auto_setup = false
require('barbar').setup({})

local opts = { noremap = true, silent = true }

vim.keymap.set("n", "<C-h>", "<cmd>BufferPrevious<cr>", opts)
vim.keymap.set("n", "<C-l>", "<cmd>BufferNext<cr>", opts)

vim.keymap.set("n", "<A-1>", "<cmd>BufferGoto 1<cr>", opts)
vim.keymap.set("n", "<A-2>", "<cmd>BufferGoto 2<cr>", opts)
vim.keymap.set("n", "<A-3>", "<cmd>BufferGoto 3<cr>", opts)
vim.keymap.set("n", "<A-4>", "<cmd>BufferGoto 4<cr>", opts)
vim.keymap.set("n", "<A-5>", "<cmd>BufferGoto 5<cr>", opts)
vim.keymap.set("n", "<A-6>", "<cmd>BufferGoto 6<cr>", opts)
vim.keymap.set("n", "<A-7>", "<cmd>BufferGoto 7<cr>", opts)
vim.keymap.set("n", "<A-8>", "<cmd>BufferGoto 8<cr>", opts)
vim.keymap.set("n", "<A-9>", "<cmd>BufferGoto 9<cr>", opts)
vim.keymap.set("n", "<A-0>", "<cmd>BufferLast<cr>", opts)

vim.keymap.set("n", "<leader>bp", "<cmd>BufferPin<cr>", opts)
vim.keymap.set("n", "<leader>bc", "<cmd>BufferClose<cr>", opts)
vim.keymap.set("n", "<leader>tp", "<cmd>BufferPick<cr>", opts)
vim.keymap.set("n", "<leader>tSb", "<cmd>BufferOrderByBufferNumber<cr>", opts)
vim.keymap.set("n", "<leader>tSd", "<cmd>BufferOrderByDirectory<cr>", opts)
vim.keymap.set("n", "<leader>tSl", "<cmd>BufferOrderByLanguage<cr>", opts)
vim.keymap.set("n", "<leader>tSw", "<cmd>BufferOrderByWindowNumber<cr>", opts)

if svenutils.isModuleAvailable("which-key") then
    local wk = require("which-key")
    wk.register({
        b = {
            name = "[b]uffer",
            p = "[p]in",
            c = "[c]lose",
        },
        t = {
            name = "[t]abs",
            p = "[p]ick",
            S = {
                name = "[S]ort",
                b = "[b]uffer number",
                d = "[d]irectory",
                l = "[l]anguage",
                w = "[w]indow",
            }
        }
    }, { prefix = "<leader>" })
end
