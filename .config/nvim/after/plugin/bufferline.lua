local lazy = require("bufferline.lazy")
local mocha = require("catppuccin.palettes").get_palette "mocha"
local svenutils = require("sven-utils")
local bufferline_groups = lazy.require("bufferline.groups")

local function project_groups()
    -- require("project_nvim").setup({})
    local project_history = require("project_nvim.utils.history")
    project_history.read_projects_from_history()
    local projects = project_history.get_recent_projects()
    local groups = {
        options = {
            toggle_hidden_on_enter = true -- when you re-enter a hidden group this options re-opens that group so the buffer is visible
        },
        items = {
            require('bufferline.groups').builtin.pinned:with({ icon = "" }),
        }
    }
    for _, project in pairs(projects) do
        local last_dir = "<none>"
        for dir in string.gmatch(project, "/[^/]*/?$") do
            last_dir = dir
        end
        last_dir = last_dir:gsub('/', '')
        table.insert(groups.items, {
            name = last_dir,
            auto_close = false,
            hightlight = {bg = mocha.lavender},
            matcher = function(buf)
                local path = vim.api.nvim_buf_get_name(buf.id)
                return path:match('.*' .. project .. '.*')
            end,
        })
    end
    if next(groups) == nil then
        groups = nil
    end
    return groups
end

local function set_groups()
    local groups = project_groups()
    bufferline_groups.setup({
        options = {
            groups = groups,
        }
    })
end

vim.defer_fn(set_groups, 100) -- HACK: I can't get project history until some async functions have finished'

vim.keymap.set('n', '<A-t>', project_groups)
require("bufferline").setup({
    options = {
        separator_style = "slant",
    },
    highlights = require("catppuccin.groups.integrations.bufferline").get {
        styles = { "italic", "bold" },
        custom = {
            all = {
                fill = { bg = mocha.crust },
            },
            mocha = {
                background = { fg = mocha.text, bg = mocha.base },
            },
            latte = {
                background = { fg = "#000000" },
            },
        },
    },
})


local opts = { noremap = true, silent = true }

vim.keymap.set("n", "<C-l>", "<cmd>BufferLineCycleNext<cr>", opts)
vim.keymap.set("n", "<C-h>", "<cmd>BufferLineCyclePrev<cr>", opts)

vim.keymap.set("n", "<A-l>", "<cmd>BufferLineMoveNext<cr>", opts)
vim.keymap.set("n", "<A-h>", "<cmd>BufferLineMovePrev<cr>", opts)

vim.keymap.set("n", "<A-1>", "<cmd>BufferLineGoToBuffer 1<cr>", opts)
vim.keymap.set("n", "<A-2>", "<cmd>BufferLineGoToBuffer 2<cr>", opts)
vim.keymap.set("n", "<A-3>", "<cmd>BufferLineGoToBuffer 3<cr>", opts)
vim.keymap.set("n", "<A-4>", "<cmd>BufferLineGoToBuffer 4<cr>", opts)
vim.keymap.set("n", "<A-5>", "<cmd>BufferLineGoToBuffer 5<cr>", opts)
vim.keymap.set("n", "<A-6>", "<cmd>BufferLineGoToBuffer 6<cr>", opts)
vim.keymap.set("n", "<A-7>", "<cmd>BufferLineGoToBuffer 7<cr>", opts)
vim.keymap.set("n", "<A-8>", "<cmd>BufferLineGoToBuffer 8<cr>", opts)
vim.keymap.set("n", "<A-9>", "<cmd>BufferLineGoToBuffer 9<cr>", opts)
vim.keymap.set("n", "<A-0>", "<cmd>BufferLineGoToBuffer 0<cr>", opts)

vim.keymap.set("n", "<leader>bP", "<cmd>BufferLineTogglePin<cr>", opts)
vim.keymap.set("n", "<leader>bc", "<cmd>bd<cr>", opts)
vim.keymap.set("n", "<A-x>", "<cmd>bd<cr>", opts)
vim.keymap.set("n", "<leader>bC", "<cmd>BufferLinePickClose<cr>", opts)
vim.keymap.set("n", "<leader>bs", "<cmd>BufferLinePick<cr>", opts)
vim.keymap.set("n", "<leader>bSt", "<cmd>BufferLineSortByTabs<cr>", opts)
vim.keymap.set("n", "<leader>bSd", "<cmd>BufferLineSortByDirectory<cr>", opts)
vim.keymap.set("n", "<leader>bSe", "<cmd>BufferLineSortByExtension<cr>", opts)

if svenutils.isModuleAvailable("which-key") then
    local wk = require("which-key")
    wk.register({
        b = {
            name = "[b]uffer",
            P = "[P]in",
            p = "[p]ick",
            c = "[c]lose",
            C = "[C]lose pick",
            s = "[s]elect",
            S = {
                name = "[S]ort",
                t = "[t]abs",
                d = "[d]irectory",
                l = "[e]xtension",
            }
        }
    }, { prefix = "<leader>" })
end
