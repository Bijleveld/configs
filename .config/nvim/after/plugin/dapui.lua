
require("mason").setup()
require("mason-nvim-dap").setup({
    ensure_installed = {"codelldb"},
    handlers = {}, -- sets up dap in the predefined manner
})

local dapui = require("dapui")
local dap = require("dap")
local svenutils = require("sven-utils")
dapui.setup()

dap.listeners.after.event_initialized["dapui_config"] = function()
  dapui.open()
end
dap.listeners.before.event_terminated["dapui_config"] = function()
  dapui.close()
end
dap.listeners.before.event_exited["dapui_config"] = function()
  dapui.close()
end

vim.keymap.set("n", "<leader>dd", function() dapui.toggle() end)
vim.keymap.set("n", "<leader>dc", function() dap.continue() end)
vim.keymap.set("n", "<leader>do", function() dap.step_over() end)
vim.keymap.set("n", "<leader>dO", function() dap.step_out() end)
vim.keymap.set("n", "<leader>di", function() dap.step_into() end)
vim.keymap.set("n", "<leader>db", function() dap.toggle_breakpoint() end)
vim.keymap.set("n", "<leader>dB", function() dap.set_breakpoint() end)
vim.keymap.set("n", "<leader>dl", function() dap.set_breakpoint(nil, nil, vim.fn.input('Log point message: ')) end)
vim.keymap.set("n", "<leader>dr", function() dap.repl.open() end)

if svenutils.isModuleAvailable("which-key") then
    local wk = require('which-key')
    wk.register({
        d = {
            d = "[d]ebug toggle",
            c = "[c]ontinue",
            o = "step [o]ver",
            O = "step [O]ut",
            i = "step [i]nto",
            b = "[b]reakpoint toggle",
            B = "[B]reakpoint",
            l = "[l]og point message",
            r = "[r]epl",
        }
    }, {prefix = "<leader>"})
end
