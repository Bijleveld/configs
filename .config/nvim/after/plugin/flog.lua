vim.keymap.set("n", "<leader>gg", "<cmd>Flog<cr>")


local svenutils = require("sven-utils")
if svenutils.isModuleAvailable("which-key") then
	local wk = require("which-key")
	wk.register({
        g = {
            g = "[g]raph"
        }
    }, {prefix = "<leader>"})
end
