vim.keymap.set("n", "<leader>gs", vim.cmd.Git)


local svenutils = require("sven-utils")
if svenutils.isModuleAvailable("which-key") then
	local wk = require("which-key")
	wk.register({
		g = {
			name = "[g]it",
			s = "[s]tatus",
		}
	}, {prefix = "<leader>"})
end
