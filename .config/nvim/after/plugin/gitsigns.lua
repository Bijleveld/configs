local gitsigns = require('gitsigns')

local function gitsigns_attach(bufnr)
    local gs = package.loaded.gitsigns

    local function map(mode, l, r, opts)
        opts = opts or {}
        opts.buffer = bufnr
        vim.keymap.set(mode, l, r, opts)
    end

    map('n', ']h', function()
        if vim.wo.diff then return ']h' end
        vim.schedule(function() gs.next_hunk() end)
        return '<Ignore>'
    end, { expr = true })

    map('n', '[h', function()
        if vim.wo.diff then return '[h' end
        vim.schedule(function() gs.prev_hunk() end)
        return '<Ignore>'
    end, { expr = true })

    map('n', '<leader>hs', gs.stage_hunk)
    map('n', '<leader>hr', gs.reset_hunk)
    map('v', '<leader>hs', function() gs.stage_hunk { vim.fn.line('.'), vim.fn.line('v') } end)
    map('v', '<leader>hr', function() gs.reset_hunk { vim.fn.line('.'), vim.fn.line('v') } end)
    map('n', '<leader>hS', gs.stage_buffer)
    map('n', '<leader>hu', gs.undo_stage_hunk)
    map('n', '<leader>hR', gs.reset_buffer)
    map('n', '<leader>hp', gs.preview_hunk)
    map('n', '<leader>hb', function() gs.blame_line { full = true } end)
    map('n', '<leader>B', gs.toggle_current_line_blame)
    map('n', '<leader>hd', gs.diffthis)
    map('n', '<leader>hD', function() gs.diffthis('~') end)
    -- map('n', '<leader>td', gs.toggle_deleted)


    local svenutils = require("sven-utils")
    if svenutils.isModuleAvailable("which-key") then
        local wk = require("which-key")
        wk.register({
            h = {
                name = "[h]unk",
                s = "[s]tage hunk",
                S = "[S]tage buffer",
                r = "[r]eset hunk",
                R = "[R]eset buffer",
                u = "[u]ndo hunk",
                p = "[p]review",
                b = "[b]lame",
                d = "[d]iff hunk",
                D = "[D]iff buffer",
            },
            B = "[B]lame",
        }, {prefix = "<leader>"})
        wk.register({
            ["["] = {
                h = "Previous [h]hunk",
            },
            ["]"] = {
                h = "Next [h]hunk",
            },
        })
    end
end

gitsigns.setup({
    on_attach = gitsigns_attach,
})
