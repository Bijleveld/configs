
local svenutils = require("sven-utils")
if not svenutils.isModuleAvailable("hop") then
    return
end
local hop = require('hop')

hop.setup({})

local directions = require('hop.hint').HintDirection
vim.keymap.set('', 'f', function()
  hop.hint_char1({ direction = directions.AFTER_CURSOR, current_line_only = true })
end, {remap=true})
vim.keymap.set('', 'F', function()
  hop.hint_char1({ direction = directions.BEFORE_CURSOR, current_line_only = true })
end, {remap=true})
vim.keymap.set('', 't', function()
  hop.hint_char1({ direction = directions.AFTER_CURSOR, current_line_only = true, hint_offset = -1 })
end, {remap=true})
vim.keymap.set('', 'T', function()
  hop.hint_char1({ direction = directions.BEFORE_CURSOR, current_line_only = true, hint_offset = 1 })
end, {remap=true})
vim.keymap.set('n', '<A-f>', '<cmd>HopWordMW<cr>')
vim.keymap.set('n', '<A-c>', '<cmd>HopAnywhereMW<cr>')

-- if svenutils.isModuleAvailable("which-key") then
-- 	local wk = require("which-key")
-- 	wk.register({})
-- end
