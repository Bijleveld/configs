local pickers = require "telescope.pickers"
local finders = require "telescope.finders"
local conf = require("telescope.config").values
local actions = require "telescope.actions"
local action_state = require "telescope.actions.state"
local themes = require("telescope.themes")
local lsp = require('lsp-zero').preset({
    float_border = 'rounded',
    call_servers = 'local',
    configure_diagnostics = true,
    setup_servers_on_start = true,
    set_lsp_keymaps = {
        preserve_mappings = false,
        omit = {},
    },
})

local function get_database_names(sqlls_client)
    local settings = sqlls_client.config.settings
    -- vim.notify(vim.inspect(settings), "info")
    if settings.sqlLanguageServer and settings.sqlLanguageServer.connections == nil then
        return nil
    end
    local connections = settings.sqlLanguageServer.connections


    local databases = {}
    for _, connection in ipairs(connections) do
        if connection.name ~= nil then
            table.insert(databases, connection.name)
        end
    end
    if next(databases) == nil then
        return nil
    end
    return databases
end


local function get_sqlls_client()
    local clients = vim.lsp.get_active_clients()
    local sqlls_client = nil
    for _, client in ipairs(clients) do
        if client.name == 'sqlls' then
            sqlls_client = client
        end
    end
    return sqlls_client
end

local function set_database(client, bufnr, dbname)
    -- vim.notify("Sending request!", "info")
    -- vim.notify(vim.inspect(sqlls_client.config), "info")
    local status, _ = client.request(
        'workspace/executeCommand',
        {
            command = "switchDatabaseConnection",
            arguments = { dbname },
        },
        function()
            -- vim.notify("handler", "info")
        end,
        bufnr
    )
    if status then
        vim.notify(string.format("Switched to database '%s'", dbname), "info")
    else
        vim.notify("switchDatabaseConnection: Failed!", "error")
    end
end

local function database_picker(databases, handler, opts)
    local opts = opts or {}
    local dbpicker = pickers.new(opts, {
        prompt_title = "Select Database",
        finder = finders.new_table({
            results = databases
        }),
        sorter = conf.generic_sorter(opts),
        attach_mappings = function(prompt_bufnr, map)
            actions.select_default:replace(function()
                actions.close(prompt_bufnr)
                local selection = action_state.get_selected_entry()
                handler(selection)
            end)
            return true
        end,
    })

    return dbpicker
end

local function change_database(bufnr, opts)
    local sqlls_client = get_sqlls_client()
    if sqlls_client == nil then
        vim.notify("No sqlls LSP client found", "error")
        return
    end

    local databases = get_database_names(sqlls_client)
    if databases == nil then
        vim.notify("No databases defined", "error")
        return
    end

    local function handler(selection)
        set_database(sqlls_client, bufnr, selection[1])
    end

    local dbpicker = database_picker(databases, handler, opts)
    dbpicker:find()
end


local lspconfig = require('lspconfig')
local util = require 'lspconfig.util'

lspconfig.lua_ls.setup(lsp.nvim_lua_ls())

lspconfig.sqlls.setup({
    -- cmd = { "sql-language-server", "up", "--method", "stdio" },
    -- filetypes = { "sql", "mysql" },
    root_dir = util.root_pattern {
        '/tmp',
        '/var/www/enigmawebsite'
    },
    settings = {
        sqlLanguageServer = {
            connections = {
            },
            lint = {
                rules = {
                    ["align-column-to-the-first"] = "error",
                    ["column-new-line"] = "off",
                    ["linebreak-after-clause-keyword"] = "off",
                    ["reserved-word-case"] = {
                        "off",
                        "upper",
                    },
                    ["space-surrounding-operators"] = "error",
                    ["where-clause-new-line"] = "off",
                    ["align-where-clause-to-the-first"] = "error",
                }
            }
        }
    },
})


lsp.ensure_installed({
    'tsserver',
    'eslint',
    'rust_analyzer',
    'sqlls',
})


lsp.on_attach(function(client, bufnr)
    lsp.default_keymaps({ buffer = bufnr })
    local opts = { buffer = bufnr, remap = false }
    local is_sqlls = client.name == 'sqlls'

    vim.keymap.set("n", "<leader>lR", function() vim.lsp.buf.rename() end, opts)
    vim.keymap.set("n", "<leader>la", function() vim.lsp.buf.code_action() end, opts)
    vim.keymap.set("n", "<leader>lf", function() vim.lsp.buf.format({ async = true }) end, opts)
    vim.keymap.set("n", "<leader>lM", "<cmd>Mason<cr>")
    vim.keymap.set("n", "<leader>lI", "<cmd>LspInfo<cr>")
    vim.keymap.set("n", "<leader>lR", "<cmd>LspRestart<cr>")
    vim.keymap.set("n", "<leader>lL", "<cmd>LspLog<cr>")

    if is_sqlls then
        vim.keymap.set("n", "<leader>ld", function() change_database(opts.buffer, themes.get_dropdown({})) end, opts)
    end

    local svenutils = require("sven-utils")
    if svenutils.isModuleAvailable("which-key") then
        local wk = require("which-key")
        if is_sqlls then
            wk.register({ l = { d = "[d]atabase picker [sqlls]" } }, {prefix = "<leader>"})
        end

        wk.register({
            l = {
                name = "[l]sp",
                c = "[c]hange symbol",
                a = "[a]ctions",
                f = "[f]ormat code",
                M = "[M]ason",
                I = "[I]nfo",
                R = "[R]estart",
                L = "[L]og",
                T = "[d]atabase [sqlls]",
            }
        }, { prefix = "<leader>" })

        wk.register({
            g = {
                d = "[d]efinition",
                D = "[D]eclaration",
                i = "[i]mplementations",
                o = "[o]rigin of type",
                r = "[r]eferences",
                s = "[s]ignature info",
                l = "[l]ist diagnostics",
            },
            ["["] = {
                d = "Previous [d]iagnostic",
            },
            ["]"] = {
                d = "Next [d]iagnostic",
            }
        })
    end
end)


local cmp = require('cmp')
local cmp_select_opts = { behavior = cmp.SelectBehavior.Select }
local cmp_action = require('lsp-zero').cmp_action()


cmp.setup({
    sources = {
        { name = 'nvim_lsp' },
        { name = 'path' },
        { name = 'buffer',  keyword_length = 3 },
        { name = 'luasnip', keyword_length = 2 },
    },
    mapping = {
        ['<Tab>'] = cmp_action.luasnip_supertab(),
        ['<S-Tab>'] = cmp_action.luasnip_shift_supertab(),
        ['<CR>'] = cmp.mapping.confirm({}),
        ['<C-y>'] = cmp.mapping.confirm({ select = true }),
        ['<C-e>'] = cmp.mapping.abort(),
        ['<C-u>'] = cmp.mapping.scroll_docs(-4),
        ['<C-d>'] = cmp.mapping.scroll_docs(4),
        ['<Up>'] = cmp.mapping.select_prev_item(cmp_select_opts),
        ['<Down>'] = cmp.mapping.select_next_item(cmp_select_opts),
        ['<C-space>'] = cmp.mapping(function()
            if cmp.visible() then
                cmp.select_prev_item(cmp_select_opts)
            else
                cmp.complete()
            end
        end),
        ['<C-n>'] = cmp.mapping(function()
            if cmp.visible() then
                cmp.select_next_item(cmp_select_opts)
            else
                cmp.complete()
            end
        end),
    },
    snippet = {
        expand = function(args)
            require('luasnip').lsp_expand(args.body)
        end,
    },
    window = {
        completion = cmp.config.window.bordered(),
        documentation = cmp.config.window.bordered(),
    },
    formatting = {
        fields = { 'abbr', 'menu', 'kind' },
        format = function(entry, item)
            local short_name = {
                nvim_lsp = 'LSP',
                nvim_lua = 'nvim'
            }

            local menu_name = short_name[entry.source.name] or entry.source.name

            item.menu = string.format('[%s]', menu_name)
            return item
        end,
    },
})

lsp.setup()
