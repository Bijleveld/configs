require('lualine').setup({
    options = {
        theme = "catppuccin"
    },
    sections = {
        lualine_x = { "overseer" },
    },
})
