local config = require('session_manager.config')

require('session_manager').setup({
    autoload_mode = config.AutoloadMode.Disabled,
})

vim.keymap.set("n", "<leader>ss", "<cmd>SessionManager load_session<cr>")
vim.keymap.set("n", "<leader>sc", "<cmd>SessionManager load_current_dir_session<cr>")
vim.keymap.set("n", "<leader>sl", "<cmd>SessionManager load_last_session<cr>")
vim.keymap.set("n", "<leader>sb", "<cmd>SessionManager save_current_session<cr>")
vim.keymap.set("n", "<leader>sd", "<cmd>SessionManager delete_session<cr>")

local config_group = vim.api.nvim_create_augroup('OverseerSession', {}) -- A global group for all your config autocommands
local function get_cwd_as_name()
    local dir = vim.fn.getcwd(0)
    return dir:gsub("[^A-Za-z0-9]", "_")
end
local overseer = require("overseer")


vim.api.nvim_create_autocmd({ 'User' }, {
    pattern = "SessionSavePre",
    group = config_group,
    callback = function()
        overseer.save_task_bundle(
            get_cwd_as_name(),
            -- Passing nil will use config.opts.save_task_opts. You can call list_tasks() explicitly and
            -- pass in the results if you want to save specific tasks.
            nil,
            { on_conflict = "overwrite" } -- Overwrite existing bundle, if any
        )
    end,
})

vim.api.nvim_create_autocmd({ 'User' }, {
    pattern = "SessionLoadPost",
    group = config_group,
    callback = function()
        overseer.load_task_bundle(get_cwd_as_name(), { ignore_missing = true })
    end,
})

vim.api.nvim_create_autocmd({ 'User' }, {
    pattern = "SessionLoadPost",
    group = config_group,
    callback = function()
        for _, task in ipairs(overseer.list_tasks({})) do
            task:dispose(true)
        end
    end,
})

local svenutils = require("sven-utils")
if svenutils.isModuleAvailable("which-key") then
    local wk = require("which-key")
    wk.register({
        s = {
            name = "[s]ession",
            s = "[s]elect",
            c = "[c]urrent dir",
            l = "[l]ast",
            b = "[b]ackup",
            d = "[d]elete",
        }
    }, { prefix = "<leader>" })
end
