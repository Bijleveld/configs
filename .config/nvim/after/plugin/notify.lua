
local notify = require("notify")

notify.setup({
    background_color = "#000000",
})
vim.keymap.set("n", "<leader>vn", "<cmd>Telescope notify<cr>")

local svenutils = require("sven-utils")
if svenutils.isModuleAvailable("which-key") then
	local wk = require("which-key")
	wk.register({
        v = {
            name = "[v]im",
            n = "[n]otifications"
        }
    }, {prefix = "<leader>"})
end

vim.notify = notify
