-- disable netrw at the very start of your init.lua
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- set termguicolors to enable highlight groups
vim.opt.termguicolors = true

local HEIGHT_RATIO = 0.8
local WIDTH_RATIO = 0.5

local builtin = require("telescope.builtin")

local actions = require("telescope.actions")
local action_state = require("telescope.actions.state")
local api = require("nvim-tree.api")

local function open_nvim_tree(prompt_bufnr, _)
    actions.select_default:replace(function()

        actions.close(prompt_bufnr)
        local selection = action_state.get_selected_entry()
        api.tree.open()
        api.tree.find_file(selection.cwd .. "/" .. selection.value)
    end)
    return true
end

local function find_directory_and_focus()
    require("telescope.builtin").find_files({
        find_command = { "fd", "--type", "directory", "--hidden", "--exclude", ".git/*" },
        attach_mappings = open_nvim_tree,
    })
end

local function find_files_and_focus()
    require("telescope.builtin").find_files({
        find_command = { "fd", "--type", "file", "--hidden", "--exclude", ".git/*" },
        attach_mappings = open_nvim_tree,
    })
end

local function on_attach(bufnr)
    local api = require "nvim-tree.api"

    local function opts(desc)
        return { desc = "nvim-tree: " .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
    end

    -- default mappings
    api.config.mappings.default_on_attach(bufnr)

    -- custom mappings
    vim.keymap.set("n", "fd", find_directory_and_focus, opts('Find Directory'))
    vim.keymap.set("n", "ff", find_files_and_focus, opts('Find Files'))
end

require("nvim-tree").setup({
    sync_root_with_cwd = true,
    respect_buf_cwd = true,
    on_attach = on_attach,
    update_focused_file = {
        enable = true,
        update_root = true,
    },
    view = {
        float = {
            enable = true,
            open_win_config = function()
                local screen_w = vim.opt.columns:get()
                local screen_h = vim.opt.lines:get() - vim.opt.cmdheight:get()
                local window_w = screen_w * WIDTH_RATIO
                local window_h = screen_h * HEIGHT_RATIO
                local window_w_int = math.floor(window_w)
                local window_h_int = math.floor(window_h)
                local center_x = (screen_w - window_w) / 2
                local center_y = ((vim.opt.lines:get() - window_h) / 2)
                    - vim.opt.cmdheight:get()
                return {
                    border = 'rounded',
                    relative = 'editor',
                    row = center_y,
                    col = center_x,
                    width = window_w_int,
                    height = window_h_int,
                }
            end,
        },
        width = function()
            return math.floor(vim.opt.columns:get() * WIDTH_RATIO)
        end,
    },
    actions = {
        open_file = {
            quit_on_open = true,
        }
    },
    filters = {
        custom = { "^.git$" }
    }
})

vim.keymap.set("n", "<leader>e", "<cmd>NvimTreeFindFileToggle<cr>")

local svenutils = require("sven-utils")
if svenutils.isModuleAvailable("which-key") then
    local wk = require("which-key")
    wk.register({
        e = { "[e]xplorer" }
    }, { prefix = "<leader>" })
end
