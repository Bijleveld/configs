require('overseer').setup({
    strategy = {
        "toggleterm",
        open_on_start = false,
    },
    templates = {
        "builtin",
        "user.c_build",
    },
})
vim.keymap.set('n', '<leader>t', "<cmd>OverseerRun<cr>")
vim.keymap.set('n', '<leader>o', "<cmd>OverseerToggle<cr>")

local svenutils = require("sven-utils")
if svenutils.isModuleAvailable("which-key") then
    local wk = require("which-key")
    wk.register({
        t = "[t]ask",
        o = "[o]verseer"
    }, { prefix = "<leader>" })
end
