vim.keymap.set("n", "<leader>Pi", "<cmd>PackerInstall<cr>")
vim.keymap.set("n", "<leader>Ps", "<cmd>PackerSync<cr>")
vim.keymap.set("n", "<leader>PS", "<cmd>PackerStatus<cr>")
vim.keymap.set("n", "<leader>Pc", "<cmd>PackerClean<cr>")
vim.keymap.set("n", "<leader>Pu", "<cmd>PackerUpdate<cr>")


local svenutils = require("sven-utils")
if svenutils.isModuleAvailable("which-key") then
    local wk = require("which-key")
    wk.register({
        P = {
            name = "[P]acker",
            i = "[i]nstall",
            s = "[s]ync",
            S = "[S]tatus",
            c = "[c]lean",
            u = "[u]pdate",
        }
    }, { prefix = "<leader>" })
end
