require("project_nvim").setup({
    detection_methods = { "pattern", "lsp" },
    patterns = {
        ".git",
        "_darcs",
        ".hg",
        ".bzr",
        ".svn",
        ".nvim_project",
        "Makefile",
        "package.json",
        "=/tmp",
    },
})

vim.keymap.set('n', '<leader>po', '<cmd>Telescope projects<cr>')


local svenutils = require("sven-utils")
if svenutils.isModuleAvailable("which-key") then
    local wk = require("which-key")
    wk.register({
        p = {
            name = "[p]roject",
            o = "[o]pen",
        }
    }, { prefix = "<leader>" })
end
