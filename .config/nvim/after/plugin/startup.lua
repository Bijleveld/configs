
local svenutils = require("sven-utils")
if not svenutils.isModuleAvailable("startup") then
    return
end


require("startup").setup({
	header = {
        type = "text",
        align = "center",
        fold_section = false,
        title = "Header",
        --margin = 5,
        content = require("user_headers").chad,
        highlight = "Statement",
        default_color = "",
        oldfiles_amount = 0,
    },
	body = {
        type = "mapping",
        align = "center",
        fold_section = false,
        title = "Basic Commands",
        --margin = 5,
        content = {
            { " [f]ile", "Telescope find_files", "f" },
            { " [b]rowse", "NvimTreeFindFileToggle", "b" },
            { " [w]ord", "Telescope live_grep", "w" },
            { " [r]ecent files", "Telescope oldfiles", "r" },
            { " [n]ew file", "lua require'startup'.new_file()", "n" },
            { " [p]roject", "lua require'telescope'.extensions.projects.projects{}", "p" },
			{ " [l]ast session", "SessionManager load_last_session", "l"},
            { " [s]ession", "SessionManager load_session", "s" }
        },
        highlight = "String",
        default_color = "",
        oldfiles_amount = 0,
    },
	body_2 = {
        type = "oldfiles",
        oldfiles_directory = true,
        align = "center",
        fold_section = false,
        title = "Oldfiles of Directory",
        --margin = 5,
        content = {},
        highlight = "String",
        default_color = "#FFFFFF",
        oldfiles_amount = 3,
    },
	options = {
        after = function()
            require("startup.utils").oldfiles_mappings()
        end,
        mapping_keys = true,
        cursor_column = 0.5,
        --[[ empty_lines_between_mappings = true, ]]
        disable_statuslines = true,
        paddings = { 1, 2, 2},
    },
    colors = {
        background = "#1f2227",
        folded_section = "#56b6c2",
    },
	parts = {
		"header",
		"body",
		"body_2",
	}
})
