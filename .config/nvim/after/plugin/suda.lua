vim.keymap.set("n", "<leader>bW", "<cmd>SudaWrite<cr>")

local svenutils = require("sven-utils")
if svenutils.isModuleAvailable("which-key") then
	local wk = require("which-key")
	wk.register({
        b = {
            W = "[W]rite as sudo"
        }
    }, {prefix = "<leader>"})
end
