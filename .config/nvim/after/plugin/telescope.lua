local actions = require("telescope.actions")
local builtin = require("telescope.builtin")

local telescope = require('telescope')
local project_nvim = require("project_nvim.project")
local previewers = require('telescope.previewers')

local new_maker = function(filepath, bufnr, opts)
  opts = opts or {}

  filepath = vim.fn.expand(filepath)
  vim.loop.fs_stat(filepath, function(_, stat)
    if not stat then return end
    if stat.size > 100000 then
      return
    else
      previewers.buffer_previewer_maker(filepath, bufnr, opts)
    end
  end)
end

telescope.setup({
    defaults = {
        buffer_previewer_maker = new_maker,
        mappings = {
            i = {
                ["<Tab>"] = actions.move_selection_previous,
                ["<S-Tab>"] = actions.move_selection_next,
            }
        }
    },
    extensions = {
        ["ui-select"] = {
            require("telescope.themes").get_dropdown({})
        }
    },
})


local function browse_project_files()
    local cwd, _, _ = project_nvim.get_project_root()
    local opt = {
        cwd = cwd,
        hidden = false,
        mode = "insert",
    }
    builtin.find_files(opt)
end


local function search_project()
    local cwd, _, _ = project_nvim.get_project_root()
    local opt = {
        cwd = cwd,
        hidden = false,
        mode = "insert",
    }
    builtin.live_grep(opt)
end

telescope.load_extension('ui-select')

vim.keymap.set('n', '<leader>pf', browse_project_files, {})
vim.keymap.set('n', '<leader>ps', search_project, {})
vim.keymap.set('n', '<C-p>', builtin.git_files, {})
vim.keymap.set('n', '<leader>pp', telescope.extensions.projects.projects, {})


vim.keymap.set('n', '<leader>bf', builtin.buffers, {})
vim.keymap.set('n', '<leader>bp', builtin.oldfiles, {})
vim.keymap.set('n', '<leader>pg', function()
    builtin.grep_string({ search = vim.fn.input("Grep > ") })
end)

vim.keymap.set('n', '<leader>gd', builtin.git_status)
vim.keymap.set('n', '<leader>gc', builtin.git_commits)
vim.keymap.set('n', '<leader>gh', builtin.git_bcommits)
vim.keymap.set('n', '<leader>gb', builtin.git_branches)
vim.keymap.set('n', '<leader>gS', builtin.git_stash)

vim.keymap.set('n', '<leader>ld', builtin.diagnostics)
vim.keymap.set('n', '<leader>ls', builtin.lsp_document_symbols)
vim.keymap.set('n', '<leader>lr', builtin.lsp_references)

vim.keymap.set('n', '<leader>vk', builtin.keymaps)
vim.keymap.set('n', '<leader>vc', builtin.commands)
vim.keymap.set('n', '<leader>vo', builtin.vim_options)
vim.keymap.set('n', '<leader>vC', builtin.colorscheme)
vim.keymap.set('n', '<leader>vm', builtin.marks)
vim.keymap.set('n', '<leader>vM', builtin.man_pages)
vim.keymap.set('n', '<leader>vr', builtin.registers)
vim.keymap.set('n', '<leader>vj', builtin.jumplist)
vim.keymap.set('n', '<leader>va', builtin.autocommands)
vim.keymap.set('n', '<leader>vf', builtin.filetypes)
vim.keymap.set('n', '<leader>vl', builtin.loclist)
vim.keymap.set('n', '<leader>vh', builtin.help_tags)
vim.keymap.set('n', '<leader>vH', builtin.highlights)
vim.keymap.set('n', '<leader>vs', builtin.search_history)

local svenutils = require("sven-utils")
if svenutils.isModuleAvailable("which-key") then
    local wk = require("which-key")
    wk.register({
        v = {
            name = "[v]im",
            k = "[k]eymaps",
            c = "[c]ommands",
            o = "[o]ptions",
            C = "[C]olorscheme",
            m = "[m]arks",
            M = "[M]an pages",
            r = "[r]egisters",
            j = "[j]umplists",
            a = "[a]utocommands",
            f = "[f]iletypes",
            l = "[l]ocation lists",
            h = "[h]elp",
            H = "[H]ighlights",
            s = "[s]earch history",
        },
        p = {
            name = "[p]roject",
            p = "[p]rojects",
            f = "[f]iles",
            s = "[s]earch",
            g = "[g]rep",
        },
        b = {
            name = "[b]uffer",
            f = "[f]ind",
            p = "[p]reviously opened",
        },
        g = {
            name = "[g]it",
            c = "[c]ommits",
            d = "[d]iff",
            h = "[h]istory",
            b = "[b]ranches",
            S = "[S]tashes",
        },
        l = {
            name = "[l]sp",
            s = "[s]ymbols",
            r = "[r]eferences",
            d = "[d]iagnostics"
        },
    }, {
        prefix = "<leader>"
    })
end
