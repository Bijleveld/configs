require('todo-comments').setup({
})

vim.keymap.set("n", "<leader>pta", "<cmd>TodoTelescope<cr>")
vim.keymap.set("n", "<leader>ptt", "<cmd>TodoTelescope keywords=TODO<cr>")
vim.keymap.set("n", "<leader>pth", "<cmd>TodoTelescope keywords=HACK<cr>")
vim.keymap.set("n", "<leader>ptn", "<cmd>TodoTelescope keywords=NOTE<cr>")
vim.keymap.set("n", "<leader>ptf", "<cmd>TodoTelescope keywords=FIX<cr>")
vim.keymap.set("n", "<leader>ptw", "<cmd>TodoTelescope keywords=WARN,WARNING<cr>")
vim.keymap.set("n", "<leader>ptp", "<cmd>TodoTelescope keywords=PERF,PERFORMANCE<cr>")
vim.keymap.set("n", "]t", function()
  require("todo-comments").jump_next()
end, { desc = "Next todo comment" })

vim.keymap.set("n", "[t", function()
  require("todo-comments").jump_prev()
end, { desc = "Previous todo comment" })



local svenutils = require("sven-utils")
if svenutils.isModuleAvailable("which-key") then
	local wk = require("which-key")
	wk.register( {
        p = {
            t = {
                name = "[t]odos",
                a = "[a]ll",
                t = "[t]odo",
                h = "[h]ack",
                n = "[n]ote",
                f = "[f]ix",
                w = "[w]arn",
                p = "[p]erf",
            }
        }
    },{prefix = "<leader>"})
end
