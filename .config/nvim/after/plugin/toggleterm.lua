require('toggleterm').setup()

local Terminal = require('toggleterm.terminal').Terminal

local term_cmd = nil
if vim.fn.executable("tmux") then
    term_cmd = "tmux new-session -DAs nvim-main"
end

local term_main_float = Terminal:new({
    count = 1,
    cmd = term_cmd,
    direction = "float",
    float_opts = {
        border = "curved",
    },

    on_open = function(term)
        vim.cmd("startinsert!")
        vim.api.nvim_buf_set_keymap(term.bufnr, "t", "<C-\\>", "<cmd>close<cr>", { noremap = true, silent = true })
        vim.api.nvim_buf_set_keymap(term.bufnr, "t", "yy", "<cmd>stopinsert<cr>", { noremap = true, silent = true })
    end,
    -- function to run on closing the terminal
    on_close = function(_)
        vim.cmd("startinsert!")
    end,
})

local function _main_float_toggle()
    term_main_float:toggle()
end

local htop_float = Terminal:new({
    count = 9,
    cmd = "htop",
    direction = "float",
    float_opts = {
        border = "curved",
    },

    on_open = function(term)
        vim.cmd("startinsert!")
        vim.api.nvim_buf_set_keymap(term.bufnr, "t", "q", "<cmd>close<CR>", { noremap = true, silent = true })
        vim.api.nvim_buf_set_keymap(term.bufnr, "t", "<C-\\>", "<cmd>close<CR>", { noremap = true, silent = true })
    end,
    -- function to run on closing the terminal
    on_close = function(_)
        vim.cmd("startinsert!")
    end,
})

local htop_exists = vim.fn.executable("htop")

local function _htop_float_toggle()
    if htop_exists then
        htop_float:toggle()
    end
end

local lazygit_float = Terminal:new({
    cmd = "lazygit",
    dir = "git_dir",
    direction = "float",
    float_opts = {
        border = "curved",
    },

    on_open = function(term)
        vim.cmd("startinsert!")
        -- vim.api.nvim_buf_set_keymap(term.bufnr, "t", "q", "<cmd>close<CR>", { noremap = true, silent = true })
        vim.api.nvim_buf_set_keymap(term.bufnr, "t", "<C-\\>", "<cmd>close<CR>", { noremap = true, silent = true })
    end,
    -- function to run on closing the terminal
    on_close = function(_)
        vim.cmd("startinsert!")
    end,
})


local lazygit_exists = vim.fn.executable("htop")

local function _lazygit_float_toggle()
    if lazygit_exists then
        lazygit_float:toggle()
    end
end

local function register_terms()
    vim.keymap.set("n", "<C-\\>", _main_float_toggle)

    local svenutils = require("sven-utils")
    local whichkey_exists = svenutils.isModuleAvailable("which-key")

    if htop_exists then
        vim.keymap.set("n", "<leader>H", _htop_float_toggle)

        if whichkey_exists then
            local wk = require("which-key")
            wk.register({
                H = "[H]top"
            }, { prefix = "<leader>" })
        end
    end

    if lazygit_exists then
        vim.keymap.set("n", "<leader>gl", _lazygit_float_toggle)
        if whichkey_exists then
            local wk = require("which-key")
            wk.register({
                g = {
                    name = "[g]it",
                    l = "[l]azygit",
                }
            }, { prefix = "<leader>" })
        end
    end
end

register_terms()
