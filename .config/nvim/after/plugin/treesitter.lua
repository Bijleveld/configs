require('nvim-treesitter.configs').setup {
    -- A list of parser names, or "all" (the five listed parsers should always be installed)
    ensure_installed = { "c", "lua", "vim", "vimdoc", "query", "rust", "javascript", "typescript", "python" },
    indent = { enable = true },

    -- Install parsers synchronously (only applied to `ensure_installed`)
    sync_install = false,

    -- Automatically install missing parsers when entering buffer
    -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
    auto_install = true,

    highlight = {
        enable = true,

        -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
        -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
        -- Using this option may slow down your editor, and you may see some duplicate highlights.
        -- Instead of true it can also be a list of languages
        additional_vim_regex_highlighting = false,
    },
}


local treesitter = require("vim.treesitter")
local embedded_sql = treesitter.query.parse_query(
    "python",
    [[
(
  [
    (
      (string_content) @sql
      (#match? @sql "^[\s \r\n]*(with|WITH|select|SELECT|update|UPDATE)+")
    )
    (call
      function: (identifier) @_funcn (#eq? @_funcn "SQL")
      (argument_list
        (string
          (string_content) @sql
        )
      )
    )
  ]
)
    ]]
)


local function get_root(bufnr)
    local parser = treesitter.get_parser(bufnr, "python", {})
    local tree = parser:parse()[1]
    return tree:root()
end


-- local function format_python_sql(bufnr)
--     bufnr = bufnr or vim.api.nvim_get_current_buffer(bufnr)
--     if vim.bo[bufnr].filetype ~= "python" then
--         vim.notify({msg = "This parser only works on python files!", level="warn"})
--         return
--     end
--
--     local root = get_root(bufnr)
--     local changes = {}
--     for id, node in embedded_sql:iter_captures(root, bufnr, 0, -1) do
--         local name = embedded_sql.captures[id]
--         if name == "sql" then
--             local range = { node:range() }
--         end
--     end
-- end
