vim.keymap.set('n', '<leader>u', vim.cmd.UndotreeToggle)


local svenutils = require("sven-utils")
if svenutils.isModuleAvailable("which-key") then
	local wk = require("which-key")
	wk.register({
		u = "[u]ndo tree"
	}, {prefix = "<leader>"})
end
