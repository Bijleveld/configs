local wk = require("which-key")

vim.o.timeout = true
vim.o.timeoutlen = 300

wk.setup({
    window = {
        border = "single"
    },
    icons = {
        group = "",
        -- separator = "",
    }
})
