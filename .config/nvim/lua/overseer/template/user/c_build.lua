return {
    name = "gcc build",
    builder = function ()
        local file = vim.fn.expand("%:p")
        local out_file = vim.fn.expand("%:r")
        return {
            cmd = {"gcc"},
            args = {
                "-o",
                out_file,
                "-g3",
                file,
            },
            components = {
                -- {"on_output_quickfix", open = true},
                "default"
            }
        }
    end,
    condition = {
        filetype = {"c"}
    }
}
