-- This file can be loaded by calling `lua require('plugins')` from your init.vim

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
    -- Packer can manage itself
    use { 'wbthomason/packer.nvim' }
    use {
        'nvim-telescope/telescope.nvim',
        tag = '0.1.1',
        requires = {
            'nvim-lua/plenary.nvim',
            'nvim-telescope/telescope-ui-select.nvim',
            {
                'nvim-telescope/telescope-fzf-native.nvim',
                run = 'make'
            },
        }
    }
    use {
        "folke/todo-comments.nvim",
        dependencies = "nvim-lua/plenary.nvim"
    }
    use { 'lewis6991/gitsigns.nvim' }
    use { 'catppuccin/nvim', as = 'catppuccin' }
    use {
        "nvim-treesitter/nvim-treesitter",
        run = ":TSUpdate | TSInstall query",
        requires = {
            "nvim-treesitter/playground",
        }
    }
    use { 'ggandor/leap.nvim' }
    use {
        'phaazon/hop.nvim',
        branch = 'v2',
    }
    use { 'Pocco81/auto-save.nvim' }
    use { "lukas-reineke/indent-blankline.nvim" }
    use { 'rcarriga/nvim-notify' }
    use { 'tpope/vim-surround' }
    use { 'tpope/vim-fugitive' }
    use { 'tpope/vim-repeat' }
    use { 'rbong/vim-flog' }
    use { 'mbbill/undotree' }
    use { 'folke/which-key.nvim' }
    use { 'Shatur/neovim-session-manager' }
    use { 'akinsho/toggleterm.nvim' }
    use { 'm4xshen/autoclose.nvim' }
    use { 'lambdalisue/suda.vim' }
    use { 'nvim-tree/nvim-tree.lua' }
    use { 'numToStr/Comment.nvim' }
    use { 'ahmedkhalf/project.nvim' }
    use { 'stevearc/overseer.nvim' }
    use { 'j-hui/fidget.nvim', tag = 'legacy' }
    use { 'anuvyklack/help-vsplit.nvim' }
    use {
        'akinsho/bufferline.nvim',
        tag = "*",
        requires = 'nvim-tree/nvim-web-devicons',
        after = "catppuccin",
    }
    use {
        'nvim-lualine/lualine.nvim',
        requires = { 'nvim-tree/nvim-web-devicons', opt = true }
    }
    use {
        "utilyre/barbecue.nvim",
        tag = "*",
        requires = {
            "SmiteshP/nvim-navic",
            "nvim-tree/nvim-web-devicons", -- optional dependency
        },
    }
    use {
        'goolord/alpha-nvim',
        requires = { 'nvim-tree/nvim-web-devicons' },
    }
    -- use {
    --     'startup-nvim/startup.nvim',
    --     requires = {
    --         'nvim-telescope/telescope.nvim',
    --         'nvim-lua/plenary.nvim'
    --     },
    -- }
    use { 'nvim-tree/nvim-web-devicons' }
    -- use {
    --     'romgrk/barbar.nvim',
    --     requires = {
    --         'lewis6991/gitsigns.nvim'
    --     }
    -- }
    use {
        'VonHeikemen/lsp-zero.nvim',
        requires = {
            -- LSP Support
            { 'neovim/nvim-lspconfig' },
            {
                'williamboman/mason.nvim',
                run = function()
                    pcall(vim.cmd, 'MasonUpdate')
                end
            },
            { 'williamboman/mason-lspconfig.nvim' },

            -- DAP
            { "mfussenegger/nvim-dap" },
            { "jay-babu/mason-nvim-dap.nvim" },

            -- Autocompletion
            { 'hrsh7th/nvim-cmp' },     -- Required
            { 'hrsh7th/cmp-nvim-lsp' }, -- Required
            { 'L3MON4D3/LuaSnip' },     -- Required
            { 'saadparwaiz1/cmp_luasnip' },
            { 'hrsh7th/cmp-buffer' },
            { 'hrsh7th/cmp-path' },
            { 'hrsh7th/cmp-cmdline' },
        }
    }
    use {
        "rcarriga/nvim-dap-ui",
        requires = {
            "mfussenegger/nvim-dap"
        }
    }
end)
