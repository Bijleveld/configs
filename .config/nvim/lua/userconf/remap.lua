vim.keymap.set("i", "jj", "<ESC>") -- rebind escape to jj

vim.keymap.set("v", "J", "<cmd>make '>+1<CR>gv=gv'")
vim.keymap.set("v", "K", "<cmd>make '<+2<CR>gv=gv'")

vim.keymap.set("n", "J", "mzJ`z")
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

vim.keymap.set("n", "<leader>r", ":%s/\\<<C-r><C-w>\\>/<C-r><C-w>/gI<Left><Left><Left>")
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex) -- default vim explorer
vim.keymap.set("n", "<leader>xv", "<cmd>vsplit<cr>")
vim.keymap.set("n", "<leader>xh", "<cmd>split<cr>")


local svenutils = require("sven-utils")
if svenutils.isModuleAvailable("which-key") then
	local wk = require("which-key")
	wk.register({
        p = {
            v = "[v]im explorer",
        },
        r = "[r]eplace word",
        x = {
            name = "[x] split",
            v = "[v]ertical split",
            h = "[h]orizontal split",
        }
    }, {prefix = "<leader>"})
end

