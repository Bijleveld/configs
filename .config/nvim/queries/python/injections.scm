(
  [
    (
      (string_content) @sql 
      (#match? @sql "^[\s \r\n\t]*(with|WITH|select|SELECT|update|UPDATE|alter|ALTER)+")
    )
    (call
      function: (identifier) @_funcn (#eq? @_funcn "SQL")
      (argument_list
        (string
          (string_content) @sql
        ) 
      )
    )
  ]
)
